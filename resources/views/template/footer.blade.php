<footer class="bg-dark text-white mt-5">
      <div class="container">
        <div class="row">
          <div class="col text-center">
            <p>Copyright &copy;LABLATORIUM E-COMMERCE 2020.</p>
          </div>
        </div>
      </div>
    </footer>